import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DetailsComponent } from './details/details.component';
import { FooGuardService } from './foo-guard.service';
import { FooResolveService } from './foo-resolve.service';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { AddNewEmployeeComponent } from './add-new-employee/add-new-employee.component';

const routes: Routes = [
  { path:'', redirectTo:'home',pathMatch:'full'},
  { path:'home', component: HomeComponent},
  { path:'add-employee', component: AddEmployeeComponent},
  {path:'add-new-employee',component:AddNewEmployeeComponent},
  { 
    path:'details/:imbdid', 
    component: DetailsComponent,
    canActivate:[FooGuardService],
    resolve:{
      movie:FooResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
