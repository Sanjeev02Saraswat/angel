import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
//using form group and form control
  constructor() { }
  employeeForm: FormGroup;
  loadEmployeeData(): void {
this.employeeForm.patchValue({

  fullName:"Sanjeev Saraswat",
  email:"Sanjeev02Saraswat@Yandex.com"
  // skills:{
  //   skillName:"C#",
  //   experience:"5",
  //   proficiency:"expert"
  // }

  // this.employeeForm.setValue({

  //   fullName:"Sanjeev Saraswat",
  //   email:"Sanjeev02Saraswat@Yandex.com"
  //   // skills:{
  //   //   skillName:"C#",
  //   //   experience:"5",
  //   //   proficiency:"expert"
  //   // }
});

  }

  ngOnInit() {
    this.employeeForm = new FormGroup({

      fullName: new FormControl(),
      email: new FormControl(),
      skills: new FormGroup({
        skillName: new FormControl(),
        experience: new FormControl(),
        proficiency: new FormControl()

      })
    });
  }

  addEmployee(): void {
    console.log(this.employeeForm.value);
    console.log(this.employeeForm.get('fullName').value);
  }

}
