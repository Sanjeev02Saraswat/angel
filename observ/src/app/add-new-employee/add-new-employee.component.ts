import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-new-employee',
  templateUrl: './add-new-employee.component.html',
  styleUrls: ['./add-new-employee.component.css']
})
export class AddNewEmployeeComponent implements OnInit {
  //using form builder
  constructor(private fb: FormBuilder) { }
  employeeForm: FormGroup;
  ngOnInit() {
    this.employeeForm = this.fb.group({
      fullName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      email: [''],
      skills: this.fb.group({
        skillName: [''],
        experience: [''],
        proficiency: ['expert']

      })
    });

    this.employeeForm.get('fullName').valueChanges.subscribe((data: string) => {
      console.log(data.length);

    });
  }

  loadEmployeeData(): void {
    this.employeeForm.patchValue({

      fullName: "Sanjeev Saraswat",
      email: "Sanjeev02Saraswat@Yandex.com",
      skills: {
        skillName: "C#",
        experience: "5",
        proficiency: "expert"
      }

      // this.employeeForm.setValue({

      //   fullName:"Sanjeev Saraswat",
      //   email:"Sanjeev02Saraswat@Yandex.com"
      //   // skills:{
      //   //   skillName:"C#",
      //   //   experience:"5",
      //   //   proficiency:"expert"
      //   // }
    });

  }

}
